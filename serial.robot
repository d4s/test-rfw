*** Settings ***
Library          SerialLibrary    encoding=ascii
Test Setup       Open Serial
Test Teardown    Delete All Ports

*** Variables ***

##
#${PORT}    socket://127.0.0.1:9000

${PORT}    loop://

### Use rfc2217 compatible proxy if needed
#${PORT}    rfc2217://127.0.0.1:9000

*** test cases ***
Wait For !
    [Documentation]    Test

    Write Data    !
    ${read} =    Read Until    terminator=!
    Log   ${read}

Hello serial test
    Write Data    Hello World!
    ${read} =     Read Until   terminator=!
    Log   ${read}

Test the read read terminator or size
    Set Port Parameter    timeout    0.1
    Set Encoding          hexlify
    ${bytes} =    Set Variable    01 23 45 0A 67 89 AB CD EF
    Write Data    ${bytes}
    Read All And Log    loglevel=info

Read Until should read until terminator or size
    Set Port Parameter    timeout    0.1
    Set Encoding          hexlify
    ${bytes} =    Set Variable
    Write Data    01 23 45 0A 67 89 AB CD EF
    ${read} =    Read Until
    Should Be Equal As Strings    ${read}    01 23 45 0A
    ${read} =    Read Until   size=2
    Should Be Equal As Strings    ${read}    67 89
    ${read} =    Read Until   terminator=CD
    Should Be Equal As Strings    ${read}    AB CD
    ${read} =    Read Until
    Should Be Equal As Strings    ${read}    EF
    ${read} =    Read Until
    Should Be Equal As Strings    ${read}    ${EMPTY}

*** Keywords ***

Open Serial
    Add Port    ${PORT}
    ...         baudrate=115200
    ...         bytesize=8
    ...         parity=N
    ...         stopbits=1
    ...         timeout=100.0
